var fetchWeather = function() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", 'http://research.engineering.wustl.edu/~todd/cse330/weather_json_static.php', true);
    xmlHttp.addEventListener("load", ajaxCallback, false);
    xmlHttp.send(null);
}

function ajaxCallback(){
    var jsonData = JSON.parse(event.target.responseText);

    document.getElementById("weatherWidget").getElementsByClassName("weather-loc")[0].innerHTML =
        "<strong>" + jsonData.location.city + "</strong>" + '  ' + jsonData.location.state;
    document.getElementById("weatherWidget").getElementsByClassName("weather-humidity")[0].textContent = jsonData.atmosphere.humidity;
    document.getElementById("weatherWidget").getElementsByClassName("weather-temp")[0].textContent = jsonData.current.temp;
    document.getElementById("weatherWidget").getElementsByClassName("weather-tomorrow")[0].src =  'http://us.yimg.com/i/us/nws/weather/gr/'+jsonData.tomorrow.code+'ds.png';
    document.getElementById("weatherWidget").getElementsByClassName("weather-dayaftertomorrow")[0].src = 'http://us.yimg.com/i/us/nws/weather/gr/'+jsonData.dayafter.code+'ds.png';
}
document.addEventListener("DOMContentLoaded", fetchWeather, false);
document.getElementById("update").addEventListener("click", fetchWeather, false);